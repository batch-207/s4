
-- A. Find all "artists" that has letter "d" in its "name".
SELECT * FROM artists WHERE name LIKE "%d%";


-- B. Find all "albums" that has letter "b" at the start of its name.
SELECT * FROM albums WHERE album_title LIKE "b%";


-- C. Join the "artists" and "albums" tables. (Where all "artists" has "a" at the start of its name.)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE artists.name LIKE "a%";


-- D. Select the "albums" sorted in "Z-A" order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;


-- E. Select the "artists" sorted in "A-Z" order. (Show only the first 5 records.)
SELECT * FROM artists ORDER BY name LIMIT 5;
